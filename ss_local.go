package sslocal

import (
	"errors"
	"fmt"
	"net"
)

type infoLocalServer struct {
	localAddr string
	localPort uint32
	protocol  string
}

func LocalServer(info infoLocalServer) error {
	listenHandle, err := net.Listen(info.protocol, info.localAddr+":"+string(info.localPort))
	if err != nil {
		errors.New("faild to listen local server")
		return err
	}
	defer listenHandle.Close()
	for {
		// Listen for an incoming connection.
		conn, err := listenHandle.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
            break
		}
		// Handle connections in a new goroutine.
		go localRequest(conn)
	}
	return nil
}

func localRequest(conn net.Conn) {
	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)
	// Read the incoming connection into the buffer.
	reqLen, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
	}
	fmt.Println(reqLen)
	// Send a response back to person contacting us.
	conn.Write([]byte("Message received."))
	// Close the connection when you're done with it.
	conn.Close()
}
